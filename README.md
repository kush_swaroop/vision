# Installing Pythoon

Since this project runs on python , the system must support it , version 3 is recommended
## Installation



1 Visit the official Python website [here](https://www.python.org/).

2 Click on the "Downloads" tab.

3 Scroll down and select the latest version of Python (e.g., Python 3.9.7).

4 On the download page, you'll see two installers: one for 32-bit and another for 64-bit.

  5 Choose the appropriate installer based on your system architecture.

6 Click on the installer to download it.

7 Once the download is complete, run the installer.

8 In the Python Installer, check the box that says "Add Python to PATH" and click on "Customize installation" if you want to modify the installation settings. Otherwise, click on "Install Now" to proceed with the default settings.

9The installer will begin the installation process. It may take a few moments to complete.
After installation, you'll see a screen that says "Setup was successful." Click on the "Close" button to exit the installer.

Python is now installed on your  system. To verify the installation, open the Command Prompt and type
```bash
 python --version. 
```
You should see the Python version number printed on the screen.


## Running the project

Import the provided Dump to MySQL Database 

Open the project in any python friendly IDE or use a command line console if preferred

Configure the database properties as per local macchine at 

```bash
Vision+\Visionplus\Visionplus\settings.py
```

Navigate to 
```bash
\Vision+\Visionplus>
```
Run
```python
manage.py runserver
```

The application can be accessed at [localhost](http://127.0.0.1:8000/)

Login using credentails admin : admin and upload the sample videos to test.

## Git

The project source can also be accessed via [Bitbucket]().