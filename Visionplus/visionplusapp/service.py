import cv2
import mediapipe as mp
import math
import pickle
import math
import numpy
import csv
import os
import pandas as pd

import numpy as np

mp_drawing = mp.solutions.drawing_utils
mp_drawing_styles = mp.solutions.drawing_styles
mp_pose = mp.solutions.pose
mp_hands = mp.solutions.hands
mp_holistic = mp.solutions.holistic


def slouch(landmarks):
    mid1_X = landmarks.landmark[10].x + landmarks.landmark[9].x
    mid2_X = landmarks.landmark[12].x + landmarks.landmark[11].x
    mid1_Y = landmarks.landmark[10].y + landmarks.landmark[9].y
    mid2_Y = landmarks.landmark[12].y + landmarks.landmark[11].y
    dist = math.dist([mid1_X, mid1_Y], [mid2_X, mid2_Y])
    if dist < 0.39:
        return True
    else:
        return False


def waist(landmarks):
    res = False
    left = landmarks.landmark[15]
    right = landmarks.landmark[16]
    leftw = landmarks.landmark[23]
    rightw = landmarks.landmark[24]
    if left.visibility > 0.5 and right.visibility > 0.5 and leftw.visibility > 0.5 and rightw.visibility > 0.5:
        if (left.x - leftw.x) <= 0.10 or (rightw.x - right.x) <= 0.09:
            if (left.x - leftw.x) >= 0 and (rightw.x - right.x) >= 0:
                res = True
    return res


def uncover(landmarks):  # need improv wrist vis hnd not etc
    res = False
    if landmarks.landmark[13].visibility > 0.5 and landmarks.landmark[11].visibility > 0.5:
        if landmarks.landmark[15].visibility <= 0.5:
            res = True
    if landmarks.landmark[12].visibility > 0.5 and landmarks.landmark[14].visibility > 0.5:
        if landmarks.landmark[16].visibility <= 0.5:
            res = True
    return res


def cal(A, B, C):
    return (C.y - A.y) * (B.x - A.x) > (B.y - A.y) * (C.x - A.x)


def intersect(A, B, C, D):
    return cal(A, C, D) != cal(B, C, D) and cal(A, B, C) != cal(A, B, D)


def angle(x, y):
    return math.atan(y / x)


def euclid(x, y):
    return math.sqrt(x * x + y * y)


def mdc(landmarks):
    return [angle(landmarks.landmark[i].x, landmarks.landmark[i].y) for i in
            range(0, 21)]


def dis(landmarks):
    l = [euclid(landmarks.landmark[i].x, landmarks.landmark[i].y) for i in
         range(0, 21)]
    return l


def frequency(landmarks_list):
    l = None
    for i in range(1, len(landmarks_list)):
        l = numpy.sort(numpy.subtract(numpy.array(landmarks_list[i - 1]), numpy.array(landmarks_list[i])))
    if l.sum() > 0:
        return l[:11].mean()
    else:
        return l[:11].mean()


def amplitude(landmarks_list):
    for i in range(1, len(landmarks_list)):
        l = numpy.sort(numpy.subtract(numpy.array(landmarks_list[i - 1]), numpy.array(landmarks_list[i])))
    return l.mean()


def process_landmarks(landmarks_list):
    landmarks_list_freq = [mdc(landmarks_list[i]) for i in range(0, len(landmarks_list))]
    landmarks_list_amp = [dis(landmarks_list[i]) for i in range(0, len(landmarks_list))]
    return frequency(landmarks_list_freq), amplitude(landmarks_list_amp)


def grade(vid_path,hand_model,face_model):
    font = cv2.FONT_HERSHEY_SIMPLEX
    fontScale = 0.85
    color = (0, 0, 0)
    thickness = 1
    imgs = []
    count = 1
    index =0
    video = cv2.VideoCapture(vid_path)
    video.set(cv2.CAP_PROP_FRAME_WIDTH, 640)
    video.set(cv2.CAP_PROP_FRAME_HEIGHT, 480)
    video.set(cv2.CAP_PROP_FPS, 30)
    score = int(video.get(cv2.CAP_PROP_FRAME_COUNT))
    initial = score
    while True:
        success, frame = video.read()
        index = index + 1
        if frame is not None:
            if index % 15 == 0 :
                score = analyze(count, score, imgs, frame, font, fontScale, color, thickness, face_model, hand_model)
        # we also need to close the video and destroy all Windows
        else:
            video.release()
            cv2.destroyAllWindows()
            return initial, score, imgs
    video.release()
    cv2.destroyAllWindows()

    return initial, score, imgs


def analyze(count, score, imgs, frame,font,fontScale,color,thickness,face_model,hand_model):
    cv2.imshow("frame", frame)
    with mp_holistic.Holistic(min_detection_confidence=0.5, min_tracking_confidence=0.5) as holistic:
        org = (0,25)
        xcord=23
        frame = frame.copy()
        frame.flags.writeable = True
        frame = cv2.cvtColor(frame, cv2.COLOR_RGB2BGR)
        results = holistic.process(frame)

        if results.pose_landmarks:
            mp_drawing.draw_landmarks(
                frame,
                results.pose_landmarks,
                mp_pose.POSE_CONNECTIONS,
                landmark_drawing_spec=mp_drawing_styles.get_default_pose_landmarks_style())
            if slouch(results.pose_landmarks):
                score = score - 1
                imgs.append(
                    cv2.putText(frame, 'Slouching', (org[0],org[1]+xcord), font, fontScale, color, thickness, cv2.LINE_AA))
                xcord = xcord + xcord
            if waist(results.pose_landmarks):
                score = score - 1
                imgs.append(
                    cv2.putText(frame, 'Hands at waist', (org[0],org[1]+xcord), font, fontScale, color, thickness, cv2.LINE_AA))
                xcord = xcord + xcord
            if uncover(results.pose_landmarks):
                score = score - 1
                imgs.append(
                    cv2.putText(frame, 'Hand hidden', (org[0],org[1]+xcord), font, fontScale, color, thickness, cv2.LINE_AA))
                xcord = xcord + xcord
            if intersect(results.pose_landmarks.landmark[14], results.pose_landmarks.landmark[16],
                           results.pose_landmarks.landmark[13], results.pose_landmarks.landmark[15]):
                score = score - 1
                imgs.append(
                    cv2.putText(frame, 'Hands crossed', (org[0],org[1]+xcord), font, fontScale, color, thickness, cv2.LINE_AA))
                xcord = xcord + xcord
            #else:
                #  score = score + 1
        if results.face_landmarks:
            face = results.face_landmarks.landmark
            face_row = list(np.array([[landmark.x, landmark.y, landmark.z] for landmark in face]).flatten())
            x = pd.DataFrame([face_row])
            body_language_class = face_model.predict(x)[0]

            if body_language_class == 'Underconfident' or body_language_class == 'Confused':
                score = score - 1
                imgs.append(cv2.putText(frame, body_language_class, (org[0],org[1]+xcord),
                                        cv2.FONT_HERSHEY_SIMPLEX, 0.85, (0, 0, 0), 1, cv2.LINE_AA))
                xcord = xcord + xcord
            #else:
              #  score = score + 1
        if results.left_hand_landmarks:
            mp_drawing.draw_landmarks(
                frame,
                results.left_hand_landmarks,
                mp_hands.HAND_CONNECTIONS,
                mp_drawing_styles.get_default_hand_landmarks_style(),
                mp_drawing_styles.get_default_hand_connections_style())
            if count == 1:
                landmark_start = results.left_hand_landmarks
            if count == 5:
                count = 0
                landmark_end = results.left_hand_landmarks
                freq_start, amp_start = process_landmarks(landmark_start)
                freq_end, amp_end = process_landmarks(landmark_end)
                freq = numpy.sort(numpy.subtract(numpy.array(freq_end), numpy.array(freq_start))).mean()
                amp = numpy.sort(numpy.subtract(numpy.array(amp_end), numpy.array(amp_start))).mean()
                data = list(freq).flatten() + list(amp).flatten()
                if hand_model.predict(data) == 'Twitching':
                    score = score - 1
                    imgs.append(
                        cv2.putText(frame, 'Hands Twitching',  (org[0],org[1]+xcord), font, fontScale, color, thickness,
                                    cv2.LINE_AA))
                    xcord = xcord + xcord
                #else:
                   # score = score + 1
        count = count + 1

        if results.right_hand_landmarks:
            mp_drawing.draw_landmarks(
                frame,
                results.right_hand_landmarks,
                mp_hands.HAND_CONNECTIONS,
                mp_drawing_styles.get_default_hand_landmarks_style(),
                mp_drawing_styles.get_default_hand_connections_style())
            if count == 1:
                landmark_start = results.right_hand_landmarks
            if count == 5:
                count = 0
                landmark_end = results.right_hand_landmarks
                freq_start, amp_start = process_landmarks(landmark_start)
                freq_end, amp_end = process_landmarks(landmark_end)
                freq = numpy.sort(numpy.subtract(numpy.array(freq_end), numpy.array(freq_start))).mean()
                amp = numpy.sort(numpy.subtract(numpy.array(amp_end), numpy.array(amp_start))).mean()
                data = list(freq).flatten() + list(amp).flatten()
                if hand_model.predict(data) == 'Twitching':
                    score = score - 1
                    imgs.append(
                        cv2.putText(frame, 'Hands Twitching',  (org[0],org[1]+xcord), font, fontScale, color, thickness,
                                    cv2.LINE_AA))
                    xcord = xcord + xcord
                #else:
                    # score = score + 1
        count = count + 1
    return score


def gradeExp(vid_path, score, imgs, model):
    font = cv2.FONT_HERSHEY_SIMPLEX
    fontScale = 12
    color = (0, 0, 0)
    thickness = 2
    video = cv2.VideoCapture(vid_path)
    count = 1
    landmark_start = []

    while True:
        success, frame = video.read()
        if frame is not None:
            cv2.imshow("frame", frame)
            with mp_holistic.Holistic(min_detection_confidence=0.5, min_tracking_confidence=0.5) as holistic:
                org = (int(0 + frame.shape[0] * 3 / 4), int(0 + frame.shape[1] * 3 / 4))
                frame = frame.copy()
                frame.flags.writeable = True
                frame = cv2.cvtColor(frame, cv2.COLOR_RGB2BGR)
                results = holistic.process(frame)
                results.pose_landmarks
                results.left_hand_landmarks
                results.right_hand_landmarks
                face = results.face_landmarks.landmark

                face_row = list(np.array([[landmark.x, landmark.y, landmark.z] for landmark in face]).flatten())
                x = pd.DataFrame([face_row])
                body_language_class = model.predict(x)[0]

                if body_language_class == 'Underconfident' or body_language_class == 'Confused':
                    score = score - 1
                    imgs.append(cv2.putText(frame, body_language_class, org,
                                            cv2.FONT_HERSHEY_SIMPLEX, 12, (255, 255, 255), 2, cv2.LINE_AA))
                #else:
                   # score = score + 1

        else:
            video.release()
            cv2.destroyAllWindows()
            return score, imgs
    video.release()
    cv2.destroyAllWindows()
    return score, imgs
