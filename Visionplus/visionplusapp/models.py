from django.db import models
from django.conf import settings


class UserRegistrationModel(models.Model):
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE)


def user_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT / user_<id>/<filename>
    return 'user_{0}/{1}'.format(instance.user.id, filename)

class Analysis(models.Model):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.PROTECT
    )
    score = models.CharField(max_length=500)


class ImageModel(models.Model):
    img = models.ImageField(upload_to=user_directory_path, null = True)
    name = models.CharField(max_length=500)
    analysis = models.ForeignKey(Analysis, on_delete=models.PROTECT, null=True)




