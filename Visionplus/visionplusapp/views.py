from django.http import request
from django.shortcuts import render
from .models import ImageModel,Analysis
from django.contrib.auth.decorators import login_required
from .forms import UserRegistration, UserEditForm, AnalysisForm
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from django.core.files.base import ContentFile
from .service import  grade
from django.core.paginator import Paginator

from PIL import Image
import io
import base64

import sklearn
import pickle


import cv2

# Create your views here.

@login_required
def dashboard(request):
    context = {
        "welcome": "Welcome to your dashboard"
    }
    return render(request, 'visionplusapp/dashboard.html', context=context)


def register(request):
    if request.method == 'POST':
        form = UserRegistration(request.POST or None)
        if form.is_valid():
            new_user = form.save(commit=False)
            new_user.set_password(
                form.cleaned_data.get('password')
            )
            new_user.save()
            return render(request, 'visionplusapp/register_done.html')
    else:
        form = UserRegistration()

    context = {
        "form": form
    }

    return render(request, 'visionplusapp/register.html', context=context)


@login_required
def edit(request):
    if request.method == 'POST':
        user_form = UserEditForm(instance=request.user,
                                 data=request.POST)
        if user_form.is_valid():
            user_form.save()
    else:
        user_form = UserEditForm(instance=request.user)
        print("here")
        print(user_form)
    context = {
        'form': user_form,
    }
    return render(request, 'visionplusapp/edit.html', context=context)

@csrf_exempt
def showvideo(request):
    request.upload_handlers.pop(0)
    return showvideo(request)

@login_required
def showvideo(request):
    with open('D:/majorProject/Vision+/R&D_POC/major/body_language.pkl', 'rb') as f:
        model = pickle.load(f)
    with open('D:/majorProject/Vision+/R&D_POC/major/face_recog.pkl', 'rb') as f1:
        facemodel = pickle.load(f1)
    user = request.user
    print(request.FILES)
    video_obj = request.FILES.get('videofile', False)
    vid_path = video_obj.temporary_file_path()
    initial,score, imgs = grade(vid_path,model,facemodel)
    # print("stage 1")
    # score, imgs = gradeHands(vid_path,score,imgs,model)
    # print("stage 2")
    # score, imgs = gradeExp(vid_path, score, imgs,facemodel)
    # print("stage 3")
    print(score)
    print(initial)
    normalized_score =round( (score / initial) *100,2)
    response =[]
    print(normalized_score)
    #print(imgs)
    for i in imgs:
        pillow_image = Image.fromarray(i)
        buffer = io.BytesIO()
        pillow_image.save(buffer, format='JPEG')
        img_str = base64.b64encode(buffer.getvalue()).decode()
        response.append("data:image/jpeg;base64,"+img_str)


    context = {'videofile': "abc",
               'form': "form",
               'score':normalized_score,
               'images': response
               }

    return render(request, 'visionplusapp/dashboard.html', context)