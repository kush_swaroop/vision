from abc import ABC, abstractmethod


class DefaultAttributeServiceInterface:

    @abstractmethod
    def data_collector(self, filepath: str, landmarks: object):
        # create a new csv file object
        # process landmarks
        # write processed landmarks into csv file
        # return csv file object?
        return

    @abstractmethod
    def process_landmarks(self, landmarks: object):
        # return processed landmarks
        return

    @abstractmethod
    def model_trainer(self, model: object, data: object, ratio: int,random: int):
        # train model according to processed landmarks
        # return trained model
        return

    @abstractmethod
    def model_tester(self, model: object, data: object, target: object):
        # return accuracy score of model
        return

    @abstractmethod
    def classifier(self, model: object, landmarks: object):
        # classify landmarks with trained model
        return

    @abstractmethod
    def model_serializer(self, model: object, filepath: str):
        # serialize model
        # return bin file/?download or path?
        return

    @abstractmethod
    def model_deserializer(self, filepath: str):
        # deserialize model
        return
