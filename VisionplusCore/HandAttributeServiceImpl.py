from VisionplusCore import DefaultAttributeServiceInterface
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score  # Accuracy metrics
import pickle
import math
import numpy
import csv
import os


class HandAttributeServiceImpl(DefaultAttributeServiceInterface):
    @staticmethod
    def angle(x, y):
        return math.atan(y / x)

    @staticmethod
    def euclid(x, y):
        return math.sqrt(x * x + y * y)

    def mdc(self, landmarks):
        return [self.angle(landmarks.landmark[i].x, landmarks.landmark[i].y) for i in
                range(0, 21)]

    def dis(self, landmarks):
        l = [self.euclid(landmarks.landmark[i].x, landmarks.landmark[i].y) for i in
             range(0, 21)]
        return l

    @staticmethod
    def frequency(landmarks_list):
        l = None
        for i in range(1, len(landmarks_list)):
            l = numpy.sort(numpy.subtract(numpy.array(landmarks_list[i - 1]), numpy.array(landmarks_list[i])))
        if l.sum() > 0:
            return l[:11].mean()
        else:
            return l[:11].mean()

    @staticmethod
    def amplitude(landmarks_list):
        for i in range(1, len(landmarks_list)):
            l = numpy.sort(numpy.subtract(numpy.array(landmarks_list[i - 1]), numpy.array(landmarks_list[i])))
        return l.mean()

    def data_collector(self, filepath_freq: str, filepath_amp: str, landmarks_start: object, landmarks_end: object,
                       class_name: str):
        head = [*range(1, 31)]  # as 36
        head.insert(0, "class")
        if os.stat(filepath_freq).st_size == 0:
            with open(filepath_freq, mode='w', newline='') as f1:
                csv_writer = csv.writer(f1, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                csv_writer.writerow(head)
        if os.stat(filepath_amp).st_size == 0:
            with open(filepath_amp, mode='w', newline='') as f1:
                csv_writer = csv.writer(f1, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                csv_writer.writerow(head)
        freq_start, amp_start = self.process_landmarks(landmarks_start)
        freq_end, amp_end = self.process_landmarks(landmarks_end)
        freq = numpy.sort(numpy.subtract(numpy.array(freq_end), numpy.array(freq_start))).mean()
        amp = numpy.sort(numpy.subtract(numpy.array(amp_end), numpy.array(amp_start))).mean()
        freq.insert(0, class_name)
        amp.insert(0, class_name)
        with open(filepath_freq, mode='a', newline='') as f1:
            csv_writer = csv.writer(f1, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            csv_writer.writerow(freq)
        with open(filepath_amp, mode='a', newline='') as f1:
            csv_writer = csv.writer(f1, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            csv_writer.writerow(amp)

    def process_landmarks(self, landmarks_list):
        landmarks_list_freq = [self.mdc(self, landmarks_list[i]) for i in range(0, len(landmarks_list))]
        landmarks_list_amp = [self.dis(self, landmarks_list[i]) for i in range(0, len(landmarks_list))]
        return self.frequency(self, landmarks_list_freq), self.amplitude(self, landmarks_list_amp)

    @staticmethod
    def model_trainer(model, data, ratio, random):
        data_x = data.drop('class', axis=1)  # features
        data_y = data['class']  # target value
        data_x_train, data_x_test, data_y_train, data_y_test = train_test_split(data_x, data_y, test_size=ratio,
                                                                                random_state=random)
        model = model.fit(data_x_train, data_y_train)
        prediction = model.predict(data_x_test)
        return model, accuracy_score(data_y_test, prediction)

    @staticmethod
    def model_tester(self, model, data, target):
        prediction = model.predict(data)
        return accuracy_score(target, prediction)

    @staticmethod
    def classifier(self, model, landmarks):
        return model.predict(landmarks)

    @staticmethod
    def model_serializer(self, model, filepath):
        with open(filepath, 'wb') as f:
            pickle.dump(model, f)

    @staticmethod
    def model_deserializer(self, filepath):
        with open(filepath, 'rb') as f:
            model = pickle.load(f)
        return model
