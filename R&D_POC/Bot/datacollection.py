
import cv2
import mediapipe as mp
from scipy.fftpack import fft, dct
import numpy
import math
import csv
import os
import pandas as pd



mp_drawing = mp.solutions.drawing_utils
mp_drawing_styles = mp.solutions.drawing_styles
mp_pose = mp.solutions.pose


global prev_mdc
final_mdc=[]
prev_mdc=[]
curr_mdc=[]
f=[]
a=[]
global prev_dis
final_dis=[]
prev_dis=[]
curr_dis=[]


freqfile = "path to freqdata.csv"
ampfile = "path to amplitutde.csv"


def angle(x,y):
  return math.atan(y/x)

def euclid(x,y):
  return math.sqrt(x*x+y*y)


def mdc(landmarks):#landmarks
  l =[angle(landmarks.landmark[i].x, landmarks.landmark[i].y) for i in (15,21)]# range( ..marks)
  return l

def dis(landmarks):#landmarks
  l = [euclid(landmarks.landmark[i].x, landmarks.landmark[i].y) for i in (15,21)]# range( ..marks)
  return l

def freq(landmarks):#landmarks
  curr_mdc = mdc(landmarks)
  global prev_mdc
  l=numpy.sort(numpy.subtract(numpy.array(prev_mdc),numpy.array(curr_mdc)))
  return l.mean()


def amp(landmarks):#landmarks
  curr_dis = dis(landmarks)
  global prev_dis
  l=numpy.sort(numpy.subtract(numpy.array(prev_dis),numpy.array(curr_dis)))

  return l.mean()

count=0
q=0
i=1
class_name = "stable"

head =[*range(1,31)]# as 36
head.insert(0,"class")

if os.stat(freqfile).st_size == 0:
    with open(freqfile, mode='w', newline='') as f1:
        csv_writer = csv.writer(f1, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        csv_writer.writerow(head)

if os.stat(ampfile).st_size == 0:
    with open(ampfile, mode='w', newline='') as f1:
        csv_writer = csv.writer(f1, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        csv_writer.writerow(head)

cap = cv2.VideoCapture(1)
with mp_pose.Pose(
    min_detection_confidence=0.5,
    min_tracking_confidence=0.5) as pose:
  while cap.isOpened():
    success, image = cap.read()
    if not success:
      print("Ignoring empty camera frame.")
      # If loading a video, use 'break' instead of 'continue'.
      continue
    #speed slow/buffering?
    # To improve performance, optionally mark the image as not writeable to
    # pass by reference.
    image.flags.writeable = False
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    results = pose.process(image)#change
    # Draw the hand annotations on the image.
    image.flags.writeable = True
    image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)

    if results.pose_landmarks:
        landmarks = results.pose_landmarks
        if  not prev_mdc:
          prev_mdc=mdc(landmarks)
          prev_mdc.sort()
          continue

        if  not prev_dis:
          prev_dis=dis(landmarks)
          prev_dis.sort()
          continue

        if count==4:#4/3#ferq change  between 1st and 4th frame
          prev_mdc = mdc(landmarks)
          prev_dis = dis(landmarks)
          count=0
          f.append(dct(final_mdc,2,5)[3])#ferq change  between 1st and 4th frame
          a.append(final_dis[3])#ferq change  between 1st and 4th frame
          final_mdc.clear()
          final_dis.clear()
          q=1
          print(len(a))
          if (len(a) == 30 and len(f) == 30):#30-25 minimum
              f.insert(0, class_name)
              a.insert(0, class_name)
              with open(freqfile, mode='a', newline='') as f1:
                  csv_writer = csv.writer(f1, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                  csv_writer.writerow(f)
              with open(ampfile, mode='a', newline='') as f1:
                  csv_writer = csv.writer(f1, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                  csv_writer.writerow(a)
              print("instance ", i," mapped")
              i=i+1
              a.clear()
              f.clear()
          continue

        mp_drawing.draw_landmarks(
            image,
            results.pose_landmarks,
            mp_pose.POSE_CONNECTIONS,
            landmark_drawing_spec=mp_drawing_styles.get_default_pose_landmarks_style())
        # Flip the image horizontally for a selfie-view display.
        cv2.imshow('MediaPipe Pose', cv2.flip(image, 1))
        final_mdc.append(round(abs(freq(landmarks)),3))#ferq change  between 1st and 4th frame
        final_dis.append(round(abs(amp(landmarks)),3))#ferq change  between 1st and 4th frame
        count=count+1
    if cv2.waitKey(10) & 0xFF == ord('q'):
      break
cap.release()

