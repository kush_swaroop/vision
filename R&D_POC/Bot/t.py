import cv2
import mediapipe as mp
import math
mp_drawing = mp.solutions.drawing_utils
mp_drawing_styles = mp.solutions.drawing_styles
mp_pose = mp.solutions.pose


def slouch(landmarks):
    mid1_X =landmarks.landmark[10].x + landmarks.landmark[9].x
    mid2_X =landmarks.landmark[12].x + landmarks.landmark[11].x
    mid1_Y =landmarks.landmark[10].y + landmarks.landmark[9].y
    mid2_Y =landmarks.landmark[12].y + landmarks.landmark[11].y
    dist = math.dist([mid1_X,mid1_Y], [mid2_X,mid2_Y])
    if(dist<0.39):
        return 'Slouching'
    else:
        return 'Straight'



def waist(landmarks):
    res=''
    left=landmarks.landmark[15]
    right =landmarks.landmark[16]
    leftw=landmarks.landmark[23]
    rightw=landmarks.landmark[24]
    if left.visibility > 0.5 and right.visibility > 0.5 and leftw.visibility > 0.5 and rightw.visibility > 0.5 :
        if (left.x - leftw.x) <=0.10 or (rightw.x - right.x)<=0.09:
            if (left.x - leftw.x) >=0 and (rightw.x - right.x)>=0:
                res= "hands at waist"
    return res

def uncover(landmarks):#need improv wrist vis hnd not etc
    res=''
    if landmarks.landmark[13].visibility > 0.5  and landmarks.landmark[11].visibility >0.5 :
        if landmarks.landmark[15].visibility <=0.5:
            res="left in pocket"
    if landmarks.landmark[12].visibility > 0.5 and landmarks.landmark[14].visibility > 0.5 :
        if landmarks.landmark[16].visibility <=0.5:
            if len(res) != 0:
                res='both hands in pocket'
            res="right in pocket"
    return res

def cal(A, B, C):
    return (C.y - A.y) * (B.x - A.x) > (B.y - A.y) * (C.x - A.x)

def intersect(A, B, C, D):
    return cal(A, C, D) != cal(B, C, D) and cal(A, B, C) != cal(A, B, D)


res=""
cap = cv2.VideoCapture(1)
with mp_pose.Pose(
    min_detection_confidence=0.5,
    min_tracking_confidence=0.5) as pose:
  while cap.isOpened():
    success, image = cap.read()
    if not success:
      print("Ignoring empty camera frame.")
      # If loading a video, use 'break' instead of 'continue'.
      continue

    # To improve performance, optionally mark the image as not writeable to
    # pass by reference.
    image.flags.writeable = False
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    results = pose.process(image)

    # Draw the pose annotation on the image.
    image.flags.writeable = True
    image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
    if results.pose_landmarks:
        #res=slouch(results.pose_landmarks)

        res=waist(results.pose_landmarks)
        # if intersect(results.pose_landmarks.landmark[14],results.pose_landmarks.landmark[16],results.pose_landmarks.landmark[13],results.pose_landmarks.landmark[15]):
        #     res='crossed arms'
        # else:
        #     res=''
        mp_drawing.draw_landmarks(
            image,
            results.pose_landmarks,
            mp_pose.POSE_CONNECTIONS,
            landmark_drawing_spec=mp_drawing_styles.get_default_pose_landmarks_style())

    img=cv2.flip(image, 1)
    cv2.putText(img, 'CLASS'
                , (95, 12), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 0), 1, cv2.LINE_AA)
    cv2.putText(img, res
                , (90, 40), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2, cv2.LINE_AA)
    cv2.imshow('MediaPipe Pose',img )
    if cv2.waitKey(10) & 0xFF == ord('q'):
      break
cap.release()