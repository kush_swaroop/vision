
import cv2
import mediapipe as mp
from scipy.spatial import ConvexHull #for area of polygon formed by wrist,thumb and tip of fingers
from scipy.fftpack import fft, dct
import numpy
import math
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib import style
import pandas as pd



mp_drawing = mp.solutions.drawing_utils
mp_drawing_styles = mp.solutions.drawing_styles
mp_hands = mp.solutions.hands#short hand annotations

global prev_mdc
final_mdc=[]
prev_mdc=[]
curr_mdc=[]
f=[]
a=[]
global prev_dis
final_dis=[]
prev_dis=[]
curr_dis=[]


freqfile = "C:\\Users\\kushs\\Desktop\\major\\freq.xlsx"
ampfile = "C:\\Users\\kushs\\Desktop\\major\\amp.xlsx"

def angle(x,y):
  return math.atan(y/x)

def euclid(x,y):
  return math.sqrt(x*x+y*y)

def mdc(multi_hand_landmarks):
  l =[angle(multi_hand_landmarks.landmark[i].x, multi_hand_landmarks.landmark[i].y) for i in range(0,21)]
  return l

def dis(multi_hand_landmarks):
  l = [euclid(multi_hand_landmarks.landmark[i].x, multi_hand_landmarks.landmark[i].y) for i in range(0, 21)]
  return l

def freq(multi_hand_landmarks):
  curr_mdc = mdc(multi_hand_landmarks)
  global prev_mdc
  l=numpy.sort(numpy.subtract(numpy.array(prev_mdc),numpy.array(curr_mdc)))
  if l.sum() > 0:
    return l[:11].mean()
  else:
    return l[:11].mean()

def amp(multi_hand_landmarks):
  curr_dis = dis(multi_hand_landmarks)
  global prev_dis
  l=numpy.sort(numpy.subtract(numpy.array(prev_dis),numpy.array(curr_dis)))

  return l.mean()

def isClosed(multi_hand_landmarks):
  l1 = []
  for i in (0, 1, 4, 8, 12, 16, 20, 0):  # wrist,thumb,tips of fingers
    l1.append([multi_hand_landmarks.landmark[i].x, multi_hand_landmarks.landmark[i].y,
               multi_hand_landmarks.landmark[i].z])  # x,y,z co ordinates of speciifc landarks

  o = (ConvexHull(numpy.array(l1)).area / 0.10)

  if o <= 0.60:
    return 0
  elif o >= 1:
    return 1
  else:
    return o



style.use('fivethirtyeight')

fig = plt.figure(figsize=(12, 6), facecolor='#DEDEDE')
ax = plt.subplot(121)
ax1 = plt.subplot(122)
ax.set_facecolor('#DEDEDE')
ax1.set_facecolor('#DEDEDE')

count=0
q=0
cap = cv2.VideoCapture(1)
with mp_hands.Hands(
    model_complexity=0,
    min_detection_confidence=0.5,
    min_tracking_confidence=0.5) as hands:
  while cap.isOpened():
    success, image = cap.read()
    if not success:
      continue
    image.flags.writeable = False
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    results = hands.process(image)
    image.flags.writeable = True
    image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
    if results.multi_hand_landmarks:
      for multi_hand_landmarks in results.multi_hand_landmarks:
        if  not prev_mdc:
          prev_mdc=mdc(multi_hand_landmarks)
          prev_mdc.sort()
          continue
        if  not prev_dis:
          prev_dis=dis(multi_hand_landmarks)
          prev_dis.sort()
          continue
        if count==5:
          prev_mdc = mdc(multi_hand_landmarks)
          prev_dis = dis(multi_hand_landmarks)
          count=0
          f.append(dct(final_mdc,2,5)[4])
          a.append(final_dis[4])
          final_mdc.clear()
          final_dis.clear()
          q=1
          continue
        mp_drawing.draw_landmarks(
          image,
          multi_hand_landmarks,
          mp_hands.HAND_CONNECTIONS,
          mp_drawing_styles.get_default_hand_landmarks_style(),
          mp_drawing_styles.get_default_hand_connections_style())
        final_mdc.append(round(abs(freq(multi_hand_landmarks)),4))
        final_dis.append(round(abs(amp(multi_hand_landmarks)),4))
        count=count+1
      img = cv2.flip(image, 1)
      cv2.putText(img, 'CLASS'
                , (95, 12), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 0), 1, cv2.LINE_AA)
      cv2.putText(img, res
                , (90, 40), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2, cv2.LINE_AA)
      cv2.imshow('MediaPipe Hands', img)





    if cv2.waitKey(5) & 0xFF == 27:
      break
cap.release()

