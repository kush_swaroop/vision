
import cv2
import mediapipe as mp
from scipy.fftpack import fft, dct
import numpy
import math
import pandas as pd
import pickle


mp_drawing = mp.solutions.drawing_utils
mp_drawing_styles = mp.solutions.drawing_styles
mp_hands = mp.solutions.hands#short hand annotations#change to pose

with open('C:/Users/kushs/Desktop/major/body_language.pkl', 'rb') as f:#path for testing
  model = pickle.load(f)

global prev_mdc
final_mdc=[]
prev_mdc=[]
curr_mdc=[]
f=[]
a=[]
global prev_dis
final_dis=[]
prev_dis=[]
curr_dis=[]

def angle(x,y):
  return math.atan(y/x)

def euclid(x,y):
  return math.sqrt(x*x+y*y)


def mdc(multi_hand_landmarks):#landmarks
  l =[angle(multi_hand_landmarks.landmark[i].x, multi_hand_landmarks.landmark[i].y) for i in range(0,21)]# range( ..marks)
  return l

def dis(multi_hand_landmarks):#landmarks
  l = [euclid(multi_hand_landmarks.landmark[i].x, multi_hand_landmarks.landmark[i].y) for i in range(0, 21)]# range( ..marks)
  return l

def freq(multi_hand_landmarks):#landmarks
  curr_mdc = mdc(multi_hand_landmarks)
  global prev_mdc
  l=numpy.sort(numpy.subtract(numpy.array(prev_mdc),numpy.array(curr_mdc)))


  if l.sum() > 0:
    return l[:11].mean()
  else:
    return l[:11].mean()

def amp(multi_hand_landmarks):#landmarks
  curr_dis = dis(multi_hand_landmarks)
  global prev_dis
  l=numpy.sort(numpy.subtract(numpy.array(prev_dis),numpy.array(curr_dis)))

  return l.mean()





count=0
q=0

res = ''
cap = cv2.VideoCapture(1)
with mp_hands.Hands(
    model_complexity=0,
    min_detection_confidence=0.5,# media pipe configs
    min_tracking_confidence=0.5) as hands:#net pose
  while cap.isOpened():
    success, image = cap.read()
    if not success:
      print("Ignoring empty camera frame.")
      # If loading a video, use 'break' instead of 'continue'.
      continue
#speed slow/buffering?
    # To improve performance, optionally mark the image as not writeable to
    # pass by reference.
    image.flags.writeable = False
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    results = hands.process(image)#change
    # Draw the hand annotations on the image.
    image.flags.writeable = True
    image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)

    if results.multi_hand_landmarks:#landmark

      for multi_hand_landmarks in results.multi_hand_landmarks:#landmark


        if  not prev_mdc:
          prev_mdc=mdc(multi_hand_landmarks)
          prev_mdc.sort()
          continue

        if  not prev_dis:
          prev_dis=dis(multi_hand_landmarks)
          prev_dis.sort()
          continue

        if count==5:#4/3
          prev_mdc = mdc(multi_hand_landmarks)
          prev_dis = dis(multi_hand_landmarks)
          count=0
          f.append(dct(final_mdc,2,5)[4])
          a.append(final_dis[4])
          final_mdc.clear()
          final_dis.clear()
          q=1
          if (len(a) == 36 and len(f) == 36):#30-25 min
              X=numpy.array(f+a).flatten()
              X = pd.DataFrame(X)
              X=X.transpose()
              body_language_class = model.predict(X)#prob?
              body_language_prob = model.predict_proba(X)#prob?
              cls=body_language_class
              prob=body_language_prob
              print(cls, prob)
              res=cls[0]
              a.clear()
              f.clear()
          continue

        mp_drawing.draw_landmarks(
          image,
          multi_hand_landmarks,
          mp_hands.HAND_CONNECTIONS,
          mp_drawing_styles.get_default_hand_landmarks_style(),
          mp_drawing_styles.get_default_hand_connections_style())#draw land marks on image
        final_mdc.append(round(abs(freq(multi_hand_landmarks)),4))
        final_dis.append(round(abs(amp(multi_hand_landmarks)),4))
        count=count+1

    img = cv2.flip(image, 1)
    cv2.putText(img, 'CLASS'
                , (95, 12), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 0), 1, cv2.LINE_AA)
    cv2.putText(img, res
                , (90, 40), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2, cv2.LINE_AA)
    cv2.imshow('MediaPipe Hands', img)

    if cv2.waitKey(10) & 0xFF == ord('q'):
      break
cap.release()

